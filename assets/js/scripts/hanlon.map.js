;(function(context) {

	var gmap;
	var CustomInfoWindow;
	var tim;
	var HTMLMarker;
	var MarkerClusterer;
	var MVVM;
	var $document = $(document);
	var $window = $(window);
	var hanlonMap;
	
	if(context) {
		HTMLMarker = context.HTMLMarker;
		MarkerClusterer = context.MarkerClusterer;
		gmap = context.gmap;
		CustomInfoWindow = context.CustomInfoWindow;
		tim = context.tim;
		MVVM = context.MVVM;
	} else {
		HTMLMarker = require('./map/html.marker.js');
		gmap = require('./gmap.js');
		CustomInfoWindow = require('./map/custom.infowindow.js');
		tim = require('./tim.microtemplate.js');
		MarkerClusterer = require('./map/google.maps.custom.clusterer.js');
		MVVM = require('./mvvm.js');
	}
	
	var formatMoney = function (str,c,d,t) {
		var n = str,
		c = isNaN(c = Math.abs(c)) ? 2 : c, 
		d = d == undefined ? "." : d, 
		t = t == undefined ? "," : t, 
		s = n < 0 ? "-" : "", 
		i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
		j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};
	
	var getSearchParams = function() {
		var params = {};
		$('#property-search-form')
			.serializeArray()
			.forEach(function(o) { params[o.name] = o.value; })
			
		return params;
	};
	
	var HanlonMap = function($el,options) {
		var self = this;
	
		this.$el = $el;
		this.$mapCanvas = this.$el.find('.map-canvas');
		this.$listCanvas = this.$el.find('.list-canvas');
		this.$responsiveInfoWindow = this.$el.find('.responsive-info-window');
		this.map = null;
		this.mvvm = new MVVM([],HanlonMap.PROPERTY_TEMPLATE,this.$listCanvas);
		this.perPage = 8;
		this.currentPage = 0;
		this.mapLoaded = $.Deferred();
		this.propertyCache = [];
		this.shouldSearchOnMove = true;
		this.openingInfoWindow = false;
		
		//add a pagination filter to mvvm
		this.mvvm.addFilter('pagination',function(index) { return index < (self.currentPage+1)*self.perPage; });
		
		this.$el.on('click','.show-more-properties',function() {
			self.currentPage++;
			self.updateListView();
		});
		
		this.viewType = HanlonMap.VIEWTYPE.MAP; //default view
		
		//instantiated on map load
		this.clusterer = null;
		this.infoWindow = null;
	
		//hack to allow our gmap module to work correctly
		this.$mapCanvas.data({center:'0,0',zoom:'12'});
		
		gmap.buildMap(this.$mapCanvas,$.extend({
			zoom: 13,
			draggable:true,
			scrollwheel:false,
			zoomControl:false,
			minZoom: 7,
			center: {lat:47.5543514,lng:-52.7244398}
		},options || {}));
		
		//listeners
		$el.on('mapLoad',function(e,map) { self.onMapLoad(map); });
		$el.on('mapZoomed',function(e) { self.onMapZoomed(); });
		
		var moveTO;
		$el.on('mapMoved',function(e) {
			
			if(self.openingInfoWindow) {
				self.openingInfoWindow = false;
				return;
			}
			
			if(self.shouldSearchOnMove) {
				clearTimeout(moveTO);
				moveTO = setTimeout(function() { self.search(getSearchParams()); },1500);
			}
		});
		
		$document
			.on('click','.map-zoom-controls button',function() {
				var 
					which = $(this).hasClass('zoom-in') ? 1 : -1;
					currentZoom = self.getMap().getZoom();
					
				self.getMap().setZoom(currentZoom+which);				
			})
			.on('click','.custom-info-window .fa-close, .responsive-info-window .fa-close',function() {  
				self.hideInfoWindow(); 
			})
			.on('click','.cluster-arrows button',function() {
				self.showInfoWindow($(this).data('id'));
			});
		
		this.search(getSearchParams());
	
	};
	
	HanlonMap.getZIndexForLatitude = function(lat) {
		return Math.round( (1/lat)*10000000);
	};
	
	HanlonMap.VIEWTYPE = {
		LIST: 1,
		MAP: 2
	};
	
	HanlonMap.RESPONSIVE_INFOWINDOW_WIDTH_AT = 650;
	
	HanlonMap.PROPERTY_TEMPLATE = '\
		<div class="col">\
			<button class="t-fa-abs fa-close">Close</button>\
			<div class="item fp-item">\
				<div class="fp-img lazybg loaded" data-src="{{thumb}}"></div>\
				<span class="fp-price d-bg">\
					<span>{{price}}</span>\
					<a href="#" rel="external" class="fp-more button">More</a>\
				</span>\
				<div class="fp-address">\
					<span class="fp-address-street">{{address1}}</span>\
					<span class="fp-address-city">{{address2}}</span>\
				</div>\
				<div class="fp-specs">\
					{{specs}}\
				</div>\
				<span class="fp-brokerage">Listed by {{listing_brokerage}}</span>\
				<div class="fp-buttons">\
					<a href="#" class="button primary fill toggle-favorite {{favoriteClass}}" data-listing-id="{{id}}">{{favoriteText}}</a>\
					<a href="{{permalink}}" class="button primary fill">View Details</a>\
				</div>\
			</div>\
		</div>';
	
	HanlonMap.prototype = {
		
		updateListView: function() {
			
			this.mvvm.setModel(this.propertyCache.map(function(property) { return property.toTemplateObject(); }));
			
			$document.trigger('updateTemplate.lazyimages');
			
			if( (this.currentPage+1)*this.perPage > this.propertyCache.length) {
				this.$el.addClass('showing-all');
			} else {
				this.$el.removeClass('showing-all');
			}
		},
		
		setViewType: function(viewType) {
			switch(viewType) {
				case HanlonMap.VIEWTYPE.LIST:
					this.viewType = HanlonMap.VIEWTYPE.LIST;
					this.$el.addClass('show-list');
					$document.trigger('updateTemplate.lazyimages');
					break;
				case HanlonMap.VIEWTYPE.MAP:
				default:
					this.viewType = HanlonMap.VIEWTYPE.MAP;
					this.$el.removeClass('show-list');
					google.maps.event.trigger(this.getMap(),'resize');
					break;
			}
		},
		
		getViewType: function() {
			switch(this.viewType) {
				case HanlonMap.VIEWTYPE.LIST:
					return HanlonMap.VIEWTYPE.LIST;
				case HanlonMap.VIEWTYPE.MAP:
				default:
					return HanlonMap.VIEWTYPE.MAP;
			}
		},
		
		hideInfoWindow: function() {
			if(this.infoWindow && this.infoWindow.isVisible()) {
				this.infoWindow.hide();
				this.infoWindow.setContent('');
				
				this.propertyCache.forEach(function(property) { property.marker.setSelected(false); });
				this.clusterer.clusters.forEach(function(cluster) {  cluster.marker && cluster.marker.setSelected(false);  });
			}
			
			this.$responsiveInfoWindow.removeClass('show');
		},
		
		getArrowHTMLForPropertyInCluster: function(property,cluster) {
			var
				nextProperty,
				prevProperty,
				html = '',
				indexOfThisProperty = cluster.markers.indexOf(property.marker),
				indexOfNextProperty = indexOfThisProperty !== cluster.markers.length-1 ? indexOfThisProperty+1 : 0,
				indexOfPrevProperty = indexOfThisProperty !== 0 ? indexOfThisProperty-1 : cluster.markers.length-1;
			
			prevProperty = this.getPropertyWithMarker(cluster.markers[indexOfPrevProperty]);
			nextProperty = this.getPropertyWithMarker(cluster.markers[indexOfNextProperty]);
			
			html += '<div class="cluster-arrows">';
			
			if(prevProperty) {
				html += tim('<button class="t-fa-abs fa-angle-left prev-arrow" title="{{address}}" data-id="{{id}}">Previous</button>',{
					address:prevProperty.getAddress(),
					id:prevProperty.getId()
				});
			}
			
			if(nextProperty) {
				html += tim('<button class="t-fa-abs fa-angle-right next-arrow" title="{{address}}" data-id="{{id}}">Next</button>',{
					address:nextProperty.getAddress(),
					id:nextProperty.getId()
				});				
			}
			
			html += '</div>';
			return html;
		},
		
		showInfoWindow: function(propertyId) {
			var property,anchor;
			
			this.propertyCache.some(function(prop) {
				if(propertyId == prop.id) {
					property = prop;
				}
				
				return property;
			});
			
			var shouldShowResponsiveInfoWindow = window.innerWidth < HanlonMap.RESPONSIVE_INFOWINDOW_WIDTH_AT;
			var html = property.getInfoWindowContent();
			this.hideInfoWindow();
			
			if(this.selectedCluster) {
				html += this.getArrowHTMLForPropertyInCluster(property,this.selectedCluster);
				!shouldShowResponsiveInfoWindow && this.selectedCluster.marker.setSelected(true);
				anchor = this.selectedCluster.marker.getPosition();
			} else {
				!shouldShowResponsiveInfoWindow && property.marker.setSelected(true);
				anchor = property.getPosition();
			}
			
			if(!shouldShowResponsiveInfoWindow) {
				this.infoWindow.setContent(html);
				this.infoWindow.open(anchor);
				
				//prevent openeing an info window from causing the map to re-search
				this.openingInfoWindow = true;
			}

			this
				.$responsiveInfoWindow
				.html(html)
				.addClass('show')
				.find('.lazybg')
				.removeClass('loaded')
				.addClass('immediate');

			$document.trigger('updateTemplate.lazyimages');
			
		},
		
		onClusterClick: function(cluster) {
			this.selectedCluster = cluster;
			this.showInfoWindow(this.getPropertiesWithMarkers(cluster.markers)[0].id);
		},
		
		onMarkerClick: function(marker) {
			this.selectedCluster = null;
			this.showInfoWindow(this.getPropertyWithMarker(marker).id);
		},
		
		getPropertyWithMarker: function(marker) {
			var property = null;
			this.propertyCache.some(function(prop) {
				if(prop.marker === marker) {
					property = prop;
				}
				return property;
			});
			
			return property;
		},
		
		getPropertiesWithMarkers: function(markers) {
			var properties = [];
			markers.forEach(function(marker) {
				properties.push(this.getPropertyWithMarker(marker));
			},this);
			
			return properties;
		},
		
		onMapLoad: function(map) {
			var self = this;
			this.map = map;
			
			//set up the clusterer
			this.clusterer = MarkerClusterer(this.getMap(),{
				zoomOnClick: false,
				markerConstructor: function(cluster) {
					
					var marker = HTMLMarker({
						map:self.getMap(),
						position:cluster.getCenter(),
						"class": "hanlon-marker"
					});
					
					//manually set the z-index
					$(marker.getMarkerDiv())
						.css('zIndex', HanlonMap.getZIndexForLatitude(marker.getPosition().lat()))
						.attr('data-count',cluster.markers.length)
						.addClass('with-count');
						
					//attach event
					google.maps.event.addListener(marker,'click',function() { this.onClusterClick(cluster); }.bind(self));
					
					return marker;
				}
				
			});
			
			//set up the infoWindow
			this.infoWindow = CustomInfoWindow({
				padding: 40,
				shouldMoveMap:true //always position the infowindow above the anchor
			},this.getMap());

			this.mapLoaded.resolve(map);
		},
		
		getMap: function() {
			return this.map ? this.map.map : null;
		},
		
		onMapZoomed: function() {
			
		},
		
		search: function(params) {
			var 
				bounds,
				self = this,
				params = params || {};
			
			if(!this.map) { 
				return this.mapLoaded.then(function() { return self.search(); }); 
			}
			
			//get the map information, if applicable
			if(this.getViewType() === HanlonMap.VIEWTYPE.MAP) {
				bounds = this.getMap().getBounds();
				params.lat_min = bounds.getSouthWest().lat();
				params.lat_max = bounds.getNorthEast().lng();
				params.lng_min = bounds.getSouthWest().lng();
				params.lng_max = bounds.getNorthEast().lng();
			}
			
			this.showLoading(true);
			
			this.searchReq && this.searchReq.abort();

			this.searchReq = $.ajax({
						url: templateJS.templateURL + '/templates/inc/i-sample-property-feed.php',
						dataType: 'json',
						data: params
					});

			return this.searchReq
					.then(function(r,status,jqXHR) {
						var dfd = $.Deferred();
						
						self.showLoading(false);
						self.propertyCache = [];
						if(r && r.properties) {
							for(var prop in r.properties) {
								self.propertyCache.push(new HanlonProperty(r.properties[prop]));
							}
						}
						dfd.resolve();
						
						return dfd.promise();
					})
					.then(function() { 
						self.displayResults(); 
					});

		},
		
		showLoading: function(show) {
			if(show) {
				this.$el.addClass('searching-map');
			} else {
				this.$el.removeClass('searching-map');
			}
		},
		
		displayResults: function() {
			window.clusterer = this.clusterer;
			
			//update the map
			this.clusterer.clearMarkers();
			
			this.propertyCache.forEach(function(property) {
				if(property.marker) {
					property.marker.setMap(null);
					google.maps.event.clearListeners(property.marker);
				}
				
				var marker = new HTMLMarker({
					position:property.getPosition(),
					map:this.getMap(),
					title:property.getAddress(),
					"class":"hanlon-marker"
				});
				
				//attach event
				google.maps.event.addListener(marker,'click',function() { this.onMarkerClick(marker); }.bind(this));
				
				//manually set the zIndex
				$(marker.getMarkerDiv()).css('zIndex', HanlonMap.getZIndexForLatitude(marker.getPosition().lat()));
				property.marker = marker;
				this.clusterer.addMarker(marker);
			},this);
			
			this.clusterer.redraw();
			
			//update the list
			this.updateListView();
		},
		
	};
	
	var HanlonProperty = function(object) {
		$.extend(this,object);
		this.position = new google.maps.LatLng(object.lat,object.lng);
	};
	
	HanlonProperty.prototype = {
		
		toTemplateObject: function() {
			return {
				id: this.getId(),
				permalink: this.getGuid(),
				address1: this.getStreet(),
				address2: this.getCity() + ', ' + this.getProvince(),
				price: this.getPrice(true),
				listing_brokerage: this.getListingBrokerage(),
				specs: this.getSpecHTML(),
				thumb: this.getThumb(),
				favoriteClass: (this.isFavorite() ? 'is-favorite' : ''),
				favoriteText: (this.isFavorite() ? 'Saved' : 'Save')
			};
		},
		
		isFavorite: function() {
			return (+this.is_favorite);
		},

		setFavorite: function(fav) {
			this.is_favorite = fav;
		},

		getPosition: function() {
			return this.position;
		},
		
		getAddress: function() {
			return this.address;
		},
		
		getThumb: function() {
			return this.images.thumb;
		},
		
		getPrice: function(formatted) {
			return formatted ? "$"+formatMoney(this.price,0) : this.price;
		},
		
		getStreet: function() {
			return this.street;
		},
		
		getCity: function() {
			return this.city;
		},
		
		getProvince: function() {
			return this.province;
		},
		
		getBathString: function() {
			if(!this.specs.bathrooms && !this.specs.halfbathrooms) {
				return 'N/A';
			}
			return this.specs.bathrooms + (this.specs.halfbathrooms ? '/'+this.specs.halfbathrooms : '');
		},
		
		getBedrooms: function() {
			
			return this.specs.bedrooms || 'N/A';
		},
		
		getInteriorSize: function() {
			return this.specs.interiorSize || 'N/A';
		},
		
		getSpecHTML: function() {
			var TMPL = '<div class="fp-spec">{{val}} <span>{{label}}</span></div>';
			var html = '';
			
			html += tim(TMPL,{val:this.getBedrooms(),label:"bedrooms"});
			html += tim(TMPL,{val:this.getBathString(),label:"bathrooms"});
			html += tim(TMPL,{val:this.getInteriorSize(),label:"Sq. Ft."});
			
			return html;
		},
		
		getListingBrokerage: function() {
			return this.list_brokerage;
		},
		
		getId: function() {
			return this.id;
		},
		
		getGuid: function() {
			return this.permalink;
		},
		
		getInfoWindowContent: function() {
			return tim(HanlonMap.PROPERTY_TEMPLATE,this.toTemplateObject());
		}
		
	};
	
	$('div.hanlon-map').each(function() {
		hanlonMap = new HanlonMap($(this));
	});
	
	$document
		.on('click','.map-button',function() {
			hanlonMap.setViewType(HanlonMap.VIEWTYPE.MAP);
		})
		.on('click','.list-button',function() {
			hanlonMap.setViewType(HanlonMap.VIEWTYPE.LIST);
		})
		.on('submit','#property-search-form',function(e) {
			var params = {};
			e.preventDefault();
			hanlonMap.search(getSearchParams());
		})
		.on('click','.property-search-handle,.property-search-close',function() {
			$('.property-search').toggleClass('show-search');
		})
		.on('toggledFavorite',function(e,id,isFav) {
			hanlonMap.propertyCache.some(function(prop) {
				if(prop.getId() == id) {
					prop.setFavorite(isFav);
					return true;
				}
			});

			//re-render the mvvm, if necessary
			if(hanlonMap.viewType === HanlonMap.VIEWTYPE.LIST) {
				hanlonMap.updateListView();
			}			
		})
		.on('click','.toggle-favorite',function() {
			var listingId = $(this).data('listingId');

			//do some ajax here

			//trigger this event when the ajax is done
			$(document)
				.trigger('toggledFavorite',[
						listingId,
						true || false //true if this property is now a favorite, false if this property is no longer a favorite
					]
				);
		})
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));