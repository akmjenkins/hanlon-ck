;(function(context) {

	var MarkerClusterer;
	var Cluster;
	
	var MarkerClustererClosure = function(map,opts) {
	
		/* private static methods */
		var distanceBetweenPoints = function(p1,p2) {
			if (!p1 || !p2) {
				return 0;
			}

			var R = 6371; // Radius of the Earth in km
			var dLat = (p2.lat() - p1.lat()) * Math.PI / 180;
			var dLon = (p2.lng() - p1.lng()) * Math.PI / 180;
			var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(p1.lat() * Math.PI / 180) * Math.cos(p2.lat() * Math.PI / 180) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			var d = R * c;
			return d;
		};
	
		var defaults = {
			markerConstructor: null,
			gridSize: 60,
			zoomOnClick:true,
			maxZoom: 21,
			averageCenter: true,
			minimumClusterSize: 2,
			onClusterClick: null
		};
	
		/**
		 * options
		 * markerConstructor: REQUIRED (function - parameters [markers] - an array of marker objects default: null - a function that, when called, returns a markerObject that implements the google.maps.OverlayView interface
		 *	gridSize: (int) default: 60 - size in pixels of the grid
		 * zoomOnClick: (boolean) default: true - zoom to display all markers in a cluster when a cluster icon is clicked
		 * maxZoom: (int) default: 21 - the maximum zoom level that a marker can be part of a cluster
		 * averageCenter: (boolean) default: true - whether the center of each cluster should be the average of all markers in the cluster
		 * minimumClusterSize: (int) default: 2 - the minimum number of markers that should be in a cluster before an individual marker is shown
		 * onClusterClick: ( function(cluster) ) default: null - custom callback function for when a cluster is clicked on - if null, map will zoom to bounds of all markers in this cluster
		 */
		 
		MarkerClusterer = function(map,opts) {
			opts = opts || defaults;
			this.clusters = [];
			this.markers = [];
			this.map = map;
			this.isReady = false;
			this.drawOnReady = false;
			this.opts = {};
			
			if(!opts.markerConstructor || typeof opts.markerConstructor !== 'function') {
				throw new Error('markerConstructor is not defined or not a function');
			}
			
			this.opts.markerConstructor = opts.markerConstructor;
			this.opts.gridSize = opts.gridSize || defaults.gridSize;
			this.opts.maxZoom = opts.maxZoom || defaults.maxZoom;
			this.opts.averageCenter = opts.averageCenter || defaults.averageCenter;
			this.opts.minimumClusterSize = opts.minimumClusterSize || defaults.minimumClusterSize;
			this.opts.onClusterClick = opts.onClusterClick || defaults.onClusterClick;
			
			//trigger onAdd to be called
			this.setMap(this.map);
			
			//on map zoom we need to redraw the clusters
			this.zoomListener = google.maps.event.addListener(this.map,'zoom_changed',this.redraw.bind(this));
		};
		
		MarkerClusterer.prototype = new google.maps.OverlayView(); /* exposes this.getProjection() which is required when calculating grid size */
		MarkerClusterer.prototype.onAdd = function() { 
			this.isReady = true; 
			this.opts.projection = this.getProjection();
			
			if(this.drawOnReady) {
				this.drawOnReady = false;
				this.redraw();
			}
			
		}; 
		MarkerClusterer.prototype.draw = function() { }; /* required stub methid on google.maps.OverlayView() */
		MarkerClusterer.prototype.onRemove = function() { 
			google.maps.event.removeListener(this.zoomListener);
		};
		
		MarkerClusterer.prototype.containsMarker = function(marker) {
			return this.markers.indexOf(marker) !== -1;
		};
		
		MarkerClusterer.prototype.addMarker = function(marker,redraw) {
			if(this.containsMarker(marker)) { return; }
			this.markers.push(marker);
			redraw && this.redraw();
		};
		
		MarkerClusterer.prototype.addMarkers = function(markers,redraw) {
			markers.forEach(function(marker,i,all) { this.addMarker(marker); },this);
			redraw && this.redraw();
		};
		
		MarkerClusterer.prototype.removeMarker = function(marker,redraw) {
			
		};
		
		MarkerClusterer.prototype.getClusterForMarker = function(marker) {
			var clusterWithMarker = null;
			this.clusters.some(function(cluster) {
				if(cluster.containsMarker(marker)) {
					clusterWithMarker = cluster;
					return true;
				}
			});
			
			return clusteWithMarker;
		};
		
		MarkerClusterer.prototype.clearMarkers = function() {
			this.clusters.forEach(function(cluster) { cluster.clear(); });
			this.clusters = [];
			this.markers = [];
		};
		
		MarkerClusterer.prototype.createClusters = function() {
			if(!this.isReady) { 
				this.drawOnReady = true;
				return; 
			}
			
			this.clusters.forEach(function(cluster) { cluster.clear(); });
			this.clusters = [];
			
			this.markers.forEach(function(marker) {
				this.addToClosestCluster(marker);
			},this);
		};
		
		MarkerClusterer.prototype.addToClosestCluster = function(marker) {
			var maxDistance = 40000;
			var clusterToAddTo = null;
			var pos = marker.getPosition();
			this.clusters.forEach(function(cluster) {
				
				var center = cluster.getCenter();
				var d = distanceBetweenPoints(center,marker.getPosition());
				
				if(center && d < maxDistance)  {
					maxDistance = d;
					clusterToAddTo = cluster;
				}
			},this);
			
			if(clusterToAddTo && clusterToAddTo.isMarkerInClusterBounds(marker)) {
				clusterToAddTo.addMarker(marker);
				return;
			}
			
			clusterToAddTo = new Cluster(this.map,this.opts);
			clusterToAddTo.addMarker(marker);
			this.clusters.push(clusterToAddTo);
			
		};
		
		MarkerClusterer.prototype.redraw = function() {
			this.createClusters();
		};
		
		Cluster = function(map,opts) {
			this.markers = [];
			this.isReady = false;
			this.map = map;
			this.center = null;
			this.bounds = null;
			this.opts = opts;
			this.marker = null;
		};
		
		Cluster.prototype.calculateBounds = function() {
			this.bounds = new google.maps.LatLngBounds();
			this.bounds.extend(this.center);
			
			var 
				p = this.opts.projection,
				neLL = new google.maps.LatLng(this.bounds.getNorthEast().lat(),this.bounds.getNorthEast().lng()),
				swLL = new google.maps.LatLng(this.bounds.getSouthWest().lat(),this.bounds.getSouthWest().lng()),
				nePoint = p.fromLatLngToDivPixel(neLL),
				swPoint = p.fromLatLngToDivPixel(swLL);
			
			nePoint.x += this.opts.gridSize;
			nePoint.y -= this.opts.gridSize;
			
			swPoint.x  -= this.opts.gridSize;
			swPoint.y += this.opts.gridSize;
			
			this.bounds.extend(p.fromDivPixelToLatLng(nePoint));
			this.bounds.extend(p.fromDivPixelToLatLng(swPoint));
			
		};
		
		Cluster.prototype.isMarkerInClusterBounds = function(marker) {
			return this.bounds.contains(marker.getPosition());
		};
		
		Cluster.prototype.clear = function() {
			if(this.marker) {
				this.marker.setMap(null);
				this.marker = null;
			}

			//remove any singly displayed markers
			this.markers.forEach(function(marker) { marker.setMap(null) });
			this.markers = [];
		};
		
		Cluster.prototype.removeMarker = function(marker) {
			var index = this.markers.indexOf(marker);
			if(index !== -1) {
				this.markers.splice(i,1);
			}
			
			this.updateIcon();
		};
		
		Cluster.prototype.addMarker = function(marker) {
			if(this.containsMarker(marker)) { return; }
			
			if(!this.center) {
				this.center = marker.getPosition();
				this.calculateBounds();
			} else {
				if(this.opts.averageCenter) {
					var l = this.markers.length + 1;
					var lat = (this.center.lat() * (l-1) + marker.getPosition().lat()) / l;
					var lng = (this.center.lng() * (l-1) + marker.getPosition().lng()) / l;
					this.center = new google.maps.LatLng(lat, lng);
					this.calculateBounds();
				}
			}
			
			//this prevents showing individual markers
			marker.setMap(null);
			this.markers.push(marker);
			
			this.updateIcon();
		};
		
		Cluster.prototype.getMarkerCount = function() {
			return this.markers.length;
		};
		
		Cluster.prototype.updateIcon = function() {
			//delete the old icon
			if(this.marker) {
				this.marker.setMap(null);
				google.maps.event.clearInstanceListeners(this.marker);
				this.marker = null;
			}
			//min cluster size not reached, show individual markers
			if(this.getMarkerCount() < this.opts.minimumClusterSize) {
				this.markers.forEach(function(marker) { marker.setMap(this.map); },this);
				return;
			}
			
			//hide the markers that were showing
			if(this.getMarkerCount() >= this.opts.minimumClusterSize) {
				this.markers.forEach(function(marker) { marker.setMap(null) });
			}
			
			//update the new icon
			this.marker = this.opts.markerConstructor(this);
			this.marker.setMap(this.map);
			
			//zoomOnClick
			this.opts.zoomOnClick && google.maps.event.addListener(this.marker,'click',function() { this.map.fitBounds(this.getBoundsOfMarkers()); }.bind(this));
		};
		
		Cluster.prototype.getBoundsOfMarkers = function() {
			var bounds = new google.maps.LatLngBounds();
			this.markers.forEach(function(marker) { 
				bounds.extend(marker.getPosition()); 
			});
			return bounds;
		};
		
		Cluster.prototype.getCenter = function() {
			return this.center;
		};
		
		Cluster.prototype.containsMarker = function(marker) {
			return this.markers.indexOf(marker) !== -1;
		};
		
		return new MarkerClusterer(map,opts);
	
	};
	
	//CommonJS
	if(typeof module !== 'undefined' && module.exports) {
		module.exports = function(map,opts) {
			return MarkerClusterer ? new MarkerClusterer(map,opts) : MarkerClustererClosure(map,opts);
		}
	//CodeKit
	} else if(context) {
		context.MarkerClusterer = function(map,opts) {
			return MarkerClusterer ? new MarkerClusterer(map,opts) : MarkerClustererClosure(map,opts);
		}
	}	

}(typeof ns !== 'undefined' ? window[ns] : undefined));