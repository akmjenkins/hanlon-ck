;(function(context) {

	var 
		methods,
		$window = $(window),
		$document = $(document),
		$html = $('html'),
		$body = $('body'),
		$nav = $('nav'),
		SHOW_CLASS = 'show-nav',
		COLLAPSE_NAV_AT = 600;

	methods = {
		
		isNavCollapsed: function() {
			return window.innerWidth < COLLAPSE_NAV_AT;
		},
	
		showNav: function(show) {
			$html[show ? 'addClass' : 'removeClass'](SHOW_CLASS);
		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $html.hasClass(SHOW_CLASS);
		}

	};
	
	//listeners
	$document
		.on('click','body',function(e) {
			$(e.target).hasClass('mobile-nav-bg') && methods.showNav(false);
		})		
		.on('click','.toggle-nav',function(e) {
			methods.toggleNav();
			return false;
		})
		.on('keydown',function(e) {
			if(e.result !== false && e.keyCode === 27) {
				if(methods.isShowingNav()) {
					methods.showNav(false);
					return false
				}
			}
		})
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));