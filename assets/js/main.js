var ns = 'JAC-HANLON';
window[ns] = {};

// Utilities used pretty much everywhere 
// @codekit-append "scripts/debounce.js"
// @codekit-append "scripts/tests.js"
// @codekit-append "scripts/image.loader.js"

// @codekit-append "scripts/tim.microtemplate.js"
// @codekit-append "scripts/map/custom.infowindow.js"
// @codekit-append "scripts/map/html.marker.js"
// @codekit-append "scripts/map/google.maps.custom.clusterer.js"
// @codekit-append "scripts/gmap.js"
// @codekit-append "scripts/mvvm.js"

// @codekit-append "scripts/hanlon.map.js"

// @codekit-append "scripts/anchors.external.popup.js"
// @codekit-append "scripts/standard.accordion.js"
// @codekit-append "scripts/magnific.popup.js"
// @codekit-append "scripts/custom.select.js"
// @codekit-append "scripts/lazy.images.js"
// @codekit-append "scripts/tabs.js"
// @codekit-append "scripts/nav.js"
// @codekit-append "scripts/swiper.js"

// @codekit-append "scripts/date.inputs.js"

// @codekit-append "global.js"