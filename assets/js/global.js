;(function(context) {

	//tooltips
	(function() {
		var methods = {
			
			getElements: function() {
				return $('.tooltip');
			},
			
			doTooltips: function() {
				this.getElements().each(function() {
					var $el = $(this);
					
					$el.data('hasTooltip') && $el.tooltipster('destroy');
					$el.tooltipster($el.data());
					$el.data('hasTooltip',true);
				});
				
			}
		};
		
		$(document) .on('ready updateTemplate.tooltip',function() { methods.doTooltips(); });
	}());
	
	//book showing
	(function() {
		
		$('.book-showing-inline-input-wrap input').on('change',function() { $('.book-showing-date-input-wrap input').val($(this).val()); });
		
	}());

	var tests;
	var debounce;
	var preventOverScroll;
	var imageLoader;
	var d;
	var swiper;
	
	if(context) {
		debounce = context.debounce;
		preventOverScroll = context.preventOverScroll;
		tests = context.tests;
		imageLoader = context.imageLoader;
		swiper = context.swiper;
	} else {
		debounce = require('./scripts/debounce.js');
		preventOverScroll = require('./scripts/preventOverScroll.js');
		tests = require('./scripts/tests.js');
		imageLoader = require('./scripts/image.loader.js');
		swiper = require('./scripts/swiper.js');
	}
	
	d = debounce();

}(typeof ns !== 'undefined' ? window[ns] : undefined));