<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper"
			data-arrows="false" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-1.jpg"></div>

					<div class="hero-content-wrap">

						<div class="hgroup">
							<h1 class="hgroup-title">New Generation of Realtors</h1>
							<span class="hgroup-subtitle">technology + engagement</span>
						</div><!-- .hgroup -->

					</div><!-- .hero-content -->

			</div><!-- .swipe-item -->
				
		</div><!-- .swiper-->
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="split-block full with-border">

			<div class="split-block-item">
				<div class="split-block-content">
					
					<h2>Home Value</h2>
					
					<p>
						To learn the value of your home, please complete the questionnaire. A Hanlon Realtor will call you to schedule your consultation.
					</p>
					
				</div><!-- .split-block-content -->
			</div><!-- .split-block-item -->
			
			<div class="split-block-item">
				<div class="split-block-content">
					
					<form action="/" class="body-form">
					
						<div class="swiper-wrapper home-value-swiper">
							<div class="swiper" data-arrows="false" data-draggable="false" data-swipe="false">
								
								<div class="swipe-item">
								
									<div class="swipe-item-content">
								
										<h2>Your Information</h2>
										
										<input type="text" name="name" placeholder="Full Name">
										<input type="email" name="email" placeholder="Email Address">
										
										<button type="button" class="button swiper-next">Next</button>
									
									</div><!-- .swipe-item-content -->
									
								</div><!-- .swipe-item -->
								
								<div class="swipe-item">
								
									<div class="swipe-item-content">
										<h2>Beds &amp; Baths</h2>
										
										<input type="text" name="beds" placeholder="# of Beds">
										<input type="text" name="baths" placeholder="# of Baths">
										
										<button type="button" class="button swiper-prev">Previous</button>
										<button type="button" class="button swiper-next">Next</button>
									</div><!-- .swipe-item-content -->
									
								</div><!-- .swipe-item -->
								
								<div class="swipe-item">
									<div class="swipe-item-content">
									
										<h2>Type of Property</h2>
										
										<div class="label-wrap">
										
											<label>
												<input type="radio" name="property_type" value="single_family">
												<span>
													<img src="../assets/images/vectors/single-family.svg" alt="house">
													Single Family
												</span>
											</label>
											
											<label>
												<input type="radio" name="property_type" value="town_house">
												<span>
													<img src="../assets/images/vectors/town-house.svg" alt="town house">
													Town House
												</span>
											</label>
											
											<label>
												<input type="radio" name="property_type" value="condo">
												<span>
													<img src="../assets/images/vectors/condo.svg" alt="condo">
													Condominium
												</span>
											</label>
										
										</div><!-- .label-wrap -->
										
										<button type="button" class="button swiper-prev">Previous</button>
										<button class="button">Submit</button>
									
									</div><!-- .swipe-item-content -->
								</div><!-- .swipe-item -->
								
								
							</div><!-- .swiper -->
						</div><!-- .swiper-wrapper -->
					
					</form><!-- .body-form -->
					
				</div><!-- .split-block-content -->
			</div><!-- .split-block-item -->
			
		</div><!-- .split-block -->
	</section>
	
	<section class="img-side-block">
		<div class="sw">
			<div class="img-side-content">
			
				<h2>Vivamus vehicula mauris leo, eu vehicula ipsum tempor non.</h2>
			
				<p>
					Pellentesque sagittis, magna sed commodo tempus, orci odio feugiat sem, id facilisis elit urna vitae elit. 
					Proin eleifend justo dui, eget bibendum urna tristique sed. Fusce malesuada, arcu ut hendrerit efficitur, 
					lorem turpis semper justo, eu feugiat sem leo eu nibh. Etiam et placerat massa, et bibendum nisi. 
				</p>
				
			</div><!-- .img-side-content -->
			
			<div class="img-side-img lazybg" data-src="../assets/images/temp/home-2.jpg">
			</div><!-- .img-side-img -->
		</div><!-- .sw -->
	</section><!-- .img-side-block -->


	<section class="d-bg primary-bg nopad">
		<div class="sw">
			<?php include('inc/i-advice-tools-inside.php'); ?>
		</div><!-- .sw -->
	</section><!-- .d-bg -->	
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>