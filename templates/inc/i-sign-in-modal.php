<div class="sign-in-modal-wrap">

	<div class="sign-in-modal">
		<div>
			<h2>Sign in to Hanlon Realty</h2>	
			
			<div class="sign-in-buttons">
				<span class="button primary fill t-fa fa-envelope-o">Sign in with email</span>
				<a href="#" rel="external" class="button fill t-fa fa-facebook">Sign in with Facebook</a>
				<a href="#" rel="external" class="button fill t-fa fa-google-plus">Sign in with Google</a>
			</div><!-- .sign-in-buttons -->
			
			<span class="block">Don't have an account yet?</span>
			<a href="#join" class="inline">Join Now &gt;</a>
			
			<small class="sign-in-disclaimer">
				By clicking Sign In with Facebook or Google, I acknowledge and agree to the Terms of Use and Privacy Policy.
			</small><!-- .block -->
			
		</div>
		<div>
			
			<form action="/" class="body-form">
				<div class="fieldset">
				
					<label>
						<span class="block">Username or E-mail</span>
						<input type="email" name="email">
					</label>
					
					<label>
						<span class="block">Password</span>
						<input type="password" name="password">
					</label>
					
					<label class="block">
						<input type="checkbox" name="remember"> Keep me signed in
					</label>
					
					<br />
					
					<button class="button">Sign In</button>
					
					<br />
					<br />
					
					<a href="#" class="inline">Forgot your username or password?</a>
				
				</div><!-- .fieldset -->
			</form><!-- .body-form -->
			
		</div>
	</div><!-- .sign-in-modal -->

</div><!-- .sign-in-modal-wrap -->