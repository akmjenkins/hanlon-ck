<div class="split-block full app-callout">

	<div class="split-block-item">
		<div class="split-block-content">
		
			<div class="app-download">
			
				<div class="app-download-content">
				
					<div class="hgroup">
						<h2 class="hgroup-title">Real Estate App</h2>
						<span class="h6-style hgroup-subtitle">Searching for the perfect home is an adventure</span>
					</div><!-- .hgroup -->
					
					<p>
						With Hanlon Realty’s Real Estate App, its as easy as pointing your phone 
						at the house you like to see its features, price and Realtor information.
					</p>
					
					<form action="/">
						<div class="fieldset">
							<input type="phone-1" maxlength="3" placeholder="709">
							<input type="phone-2" maxlength="3" placeholder="690">
							<input type="phone-3" maxlength="4" placeholder="1234">
							<button class="button">Download</button>
						</div><!-- .fieldset -->
						<small>Canada mobile numbers only. Message and data rates may vary.</small>
					</form>
					
					<a href="#" rel="external" class="lazybg app-store-badge" data-src="../assets/images/app-store.svg">Download from the App Store</a>
				
				</div><!-- .app-download-content -->
				
			</div><!-- .download-app -->

		</div><!-- .split-block-content -->
	</div><!-- .split-block-item -->

	<div class="split-block-item">
		<div class="split-block-content">
		
			<div class="app-features">
				
				<div class="grid">
				
					<div class="col">
						<div class="item">
							
							<h6 class="app-feature-title">Add to Favourites</h6>
							<i class="t-fa fa-heart-o ico"></i>
							
							<p>Easily keep track of listings that are important to you.</p>
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col">
						<div class="item">
							
							<h6 class="app-feature-title">Quick Details</h6>
							<i class="t-fa fa-check ico"></i>
							
							<p>List of features and options 2 beds, washer/dryer?</p>
							
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col">
						<div class="item">
							
							<h6 class="app-feature-title">Connect</h6>
							<i class="t-fa fa-comments ico"></i>
							
							<p>Ability to quickly connect with one of our full-service Realtors</p>
							
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->
				
				<a href="#" class="button">Learn More</a>
				
			</div><!-- .app-features -->
		
		</div><!-- .split-block-content -->
	</div><!-- .split-block-item -->	

</div><!-- .split-block -->