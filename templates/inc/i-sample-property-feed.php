<?php
    $properties = array(
		'2552' => array(
            "id"=> 2552,
            "price"=> 2124300,
			"street" =>"0 Whelans Crescent",
			"city" =>"Paradise",
			"province" => "NL",
            "address"=> "0 Whelans Crescent , Paradise",
            "lat"=> 47.5649,
            "lng"=> -52.89727,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1418083200, 
            "description"=> "Earth has not anything to show more beautiful. One of a larger section of land  overlooking Conception Bay & Bell Island. Located in Paradise off St. Thomas...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/listing/0-whelans-crescent-paradise/",
            "specs"=> array(
                "age"=> 0,
                "bedrooms"=> 0,
                "bathrooms"=> 0,
                "halfbathrooms"=> 0,
                "building_type"=> "",
                "style"=> "Other",
                "exterior_size"=> "",
                "heating_type"=> "No heat",
                "interior_size"=> ""
            )
		),
        "2553"=> array(
            "id"=> 2553,
            "price"=> 7314600,
			"street" =>"0 Whelans Crescent",
			"city" =>"Paradise",
			"province" => "NL",
            "address"=> "0 Whelans Crescent , Paradise",
            "lat"=> 47.5649,
            "lng"=> -52.89727,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1418083200,
            "description"=> "Earth has not anything to show more beautiful. One of a larger section of land  overlooking Conception Bay & Bell Island. Located in Paradise off St. Thomas...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/listing/0-whelans-crescent-paradise-2/",
            "specs"=> array(
                "age"=> 0,
                "bedrooms"=> 0,
                "bathrooms"=> 0,
                "halfbathrooms"=> 0,
                "building_type"=> "",
                "style"=> "Other",
                "exterior_size"=> "",
                "heating_type"=> "No heat",
                "interior_size"=> ""
            )
        ),
        "7126"=> array(
            "id"=> 7126,
            "price"=> 292900,
			"street" =>"0 Southern Point Road",
			"city" =>"Hearts Content",
			"province" => "NL",
            "address"=> "0 Southern Point Road , Hearts Content",
            "lat"=> 47.88149,
            "lng"=> -53.37958,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1421366400,
            "description"=> "Ocean view Panoramic on this 32 acre pristine property. Located in the historic town of Hearts Content. Beautiful location for a golf course, subdivision,...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/listing/0-southern-point-road-hearts-content/",
            "specs"=> array(
                "age"=> 0,
                "bedrooms"=> 0,
                "bathrooms"=> 0,
                "halfbathrooms"=> 0,
                "building_type"=> "",
                "style"=> "Other",
                "exterior_size"=> "",
                "heating_type"=> "No heat",
                "interior_size"=> ""
            )
        ),
        "10158"=> array(
            "id"=> 10158,
            "price"=> 59900,
			"street" =>"0 Brazils Road",
			"city" =>"Old Shop",
			"province" => "NL",
            "address"=> "0 Brazils Road , Old Shop",
            "lat"=> 47.51758,
            "lng"=> -53.55942,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1421366400,
            "description"=> "Spectacular ocean view building lots. Dream come true. Survey available.",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/listing/0-brazils-road-old-shop/",
            "specs"=> array(
                "age"=> 0,
                "bedrooms"=> 0,
                "bathrooms"=> 0,
                "halfbathrooms"=> 0,
                "building_type"=> "",
                "style"=> "Other",
                "exterior_size"=> "",
                "heating_type"=> "No heat",
                "interior_size"=> ""
            )
        ),
        "10374"=> array(
            "id"=> 10374,
            "price"=> 975000,
			"street" =>"0 Long Harry Road",
			"city" =>"Bell Island",
			"province" => "NL",
            "address"=> "0 Long Harry Road , Bell Island",
            "lat"=> 47.64737,
            "lng"=> -52.92141,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1409356800,
            "description"=> "Approximately 50 acres of previous farmland known as the Dwyer Property. Approximately 1500 feet raod frontage and 2000 feet of ocean frontage which extends...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/listing/0-long-harry-road-bell-island/",
            "specs"=> array(
                "age"=> 0,
                "bedrooms"=> 0,
                "bathrooms"=> 0,
                "halfbathrooms"=> 0,
                "building_type"=> "",
                "style"=> "",
                "exterior_size"=> "",
                "heating_type"=> "",
                "interior_size"=> ""
            )
        ),
        "10999"=> array(
            "id"=> 10999,
            "price"=> 29900,
			"street" =>"21a Bryants Cove Rd",
			"city" =>"Upper Island Cove",
			"province" => "NL",
            "address"=> "21a Bryants Cove Rd, Upper Island Cove",
            "lat"=> 47.65149,
            "lng"=> -53.21627,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1445731200,
            "description"=> "Approx 3.3 acres - identified by a stone wall with access off Bryants Cove Road(just off Main Road & access from the cemetery) also off valley road. This...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/listing/21a-bryants-cove-rd-upper-island-cove/",
            "specs"=> array(
                "age"=> 0,
                "bedrooms"=> 0,
                "bathrooms"=> 0,
                "halfbathrooms"=> 0,
                "building_type"=> "",
                "style"=> "Other",
                "exterior_size"=> "",
                "heating_type"=> "No heat",
                "interior_size"=> ""
            )
        ),
        "11127"=> array(
            "id"=> 11127,
            "price"=> 375000,
			"street" =>"661-699 Torbay Rd.",
			"city" =>"St. John's",
			"province" => "NL",
            "address"=> "661-699 Torbay Rd, St. John's",
            "lat"=> 47.620277,
            "lng"=> -52.723488,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1424822400,
            "description"=> "Land for sale in St. Johns newest power centre; The Field Farm Power Centre is conveniently located minutes from the Outer Ring Road/TCH, 10 minutes from...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/listing/661-699-torbay-rd-st-johns/",
            "specs"=> array(
                "age"=> 0,
                "bedrooms"=> 0,
                "bathrooms"=> 0,
                "halfbathrooms"=> 0,
                "building_type"=> "Retail",
                "style"=> "",
                "exterior_size"=> "",
                "heating_type"=> "No heat",
                "interior_size"=> ""
            )
        ),
        "11293"=> array(
            "id"=> 11293,
            "price"=> 324900,
			"street" =>"1a Country Lane",
			"city" =>"Blaketown",
			"province" => "NL",
            "address"=> "1a Country Lane, Blaketown",
            "lat"=> 47.73137,
            "lng"=> -53.36748,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1422316800,
            "description"=> "***ALL PHOTOS ARE SAMPLE..!!!*** New 2 Apartment home with separate utilities (2 meter electrical system), to  be built to National Building Code standards...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/listing/1a-country-lane-blaketown/",
            "specs"=> array(
                "age"=> 0,
                "bedrooms"=> 5,
                "bathrooms"=> 3,
                "halfbathrooms"=> 0,
                "building_type"=> "House",
                "style"=> "Detached",
                "exterior_size"=> "",
                "heating_type"=> "Baseboard heaters",
                "interior_size"=> "2436 sqft"
            )
        ),
        "11734"=> array(
            "id"=> 11734,
            "price"=> 496900,
			"street" =>"13 Titania Place",
			"city" =>"St. John's",
			"province" => "NL",
            "address"=> "13 Titania Place, St. John's",
            "lat"=> 47.545652242764,
            "lng"=> -52.7991771698,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1380758400,
            "description"=> "Close to completion. Lovely new 2 storey with an attached garage. Spacious open concept with large walk-in pantry off kitchen, 3 bedrooms, master features...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/featured-listing/13-titania-place-st-johns/",
            "specs"=> array(
                "age"=> "New",
                "bedrooms"=> 3,
                "bathrooms"=> 2,
                "halfbathrooms"=> 1,
                "building_type"=> null,
                "style"=> "2 Level",
                "exterior_size"=> null,
                "heating_type"=> "Electric",
                "interior_size"=> 3105
            )
        ),
        "11808"=> array(
            "id"=> 11808,
            "price"=> 219900,
			"street" =>"16 New Road",
			"city" =>"Pouch Cove",
			"province" => "NL",
            "address"=> "16 New Road, Pouch Cove",
            "lat"=> 47.768362272871,
            "lng"=> -52.769136428833,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1380758400,
            "description"=> "Easy living in beautiful Pouch Cove awaits you!  This split-entry home features 2 bedrooms, 2 bathrooms with ensuite and walk in closet off the master,...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/featured-listing/16-new-road-pouch-cove/",
            "specs"=> array(
                "age"=> "New",
                "bedrooms"=> 2,
                "bathrooms"=> 2,
                "halfbathrooms"=> 0,
                "building_type"=> null,
                "style"=> "Split Entry",
                "exterior_size"=> null,
                "heating_type"=> "",
                "interior_size"=> 977
            )
        ),
        "11812"=> array(
            "id"=> 11812,
            "price"=> 215900,
			"street" =>"14 New Road",
			"city" =>"Pouch Cove",
			"province" => "NL",
            "address"=> "14 New Road, Pouch Cove",
            "lat"=> 47.768804,
            "lng"=> -52.76849,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1380758400,
            "description"=> "Nestled in the heart of scenic Pouch Cove, this split entry property is the perfect home for the first time buyer or young family.  Graciously priced and...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/featured-listing/14-new-road-pouch-cove/",
            "specs"=> array(
                "age"=> "New",
                "bedrooms"=> 3,
                "bathrooms"=> 1,
                "halfbathrooms"=> 0,
                "building_type"=> null,
                "style"=> "Split Entry",
                "exterior_size"=> null,
                "heating_type"=> "",
                "interior_size"=> 955
            )
        ),
        "18940"=> array(
            "id"=> 18940,
            "price"=> 357500,
			"street" =>"16 Duke Street",
			"city" =>"St. John's",
			"province" => "NL",
            "address"=> "16 Duke Street, St. John's",
            "lat"=> 47.548464775806,
            "lng"=> -52.797932624817,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1400025600,
            "description"=> "Lovely new bungalow located in Westgate Subdivision, just minutes to Kelsey Drive amenities.  Nice open concept, 3 bedrooms, spacious master bedroom with...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/featured-listing/16-duke-street-st-johns/",
            "specs"=> array(
                "age"=> "New",
                "bedrooms"=> 3,
                "bathrooms"=> 1,
                "halfbathrooms"=> 1,
                "building_type"=> null,
                "style"=> "Split Entry",
                "exterior_size"=> null,
                "heating_type"=> "Res",
                "interior_size"=> 2574
            )
        ),
        "21030"=> array(
            "id"=> 21030,
            "price"=> 384900,
			"street" =>"504 Main Road",
			"city" =>"Pouch Cove",
			"province" => "NL",
            "address"=> "504 Main Road, Pouch Cove",
            "lat"=> 47.753405513132,
            "lng"=> -52.743644714355,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1403654400,
            "description"=> "Looking for that affordable executive home? Look no further! Just 2 years old, this home is fully developed with quality finishes throughout. Two hardwood...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/featured-listing/504-main-road-pouch-cove/",
            "specs"=> array(
                "age"=> 2,
                "bedrooms"=> 4,
                "bathrooms"=> 3,
                "halfbathrooms"=> 1,
                "building_type"=> null,
                "style"=> "2 Level",
                "exterior_size"=> null,
                "heating_type"=> "Electric",
                "interior_size"=> 2450
            )
        ),
        "22583"=> array(
            "id"=> 22583,
            "price"=> 263900,
			"street" =>"8 Rhaye Place",
			"city" =>"St. John's",
			"province" => "NL",
            "address"=> "8 Rhaye Place, St. John\'s",
            "lat"=> 47.614300241791,
            "lng"=> -52.726210355759,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1405036800,
            "description"=> "This is your chance to buy a beautiful ENERGY EFFICIENT HOME AT A PRICE YOU CAN AFFORD. WE GUARANTEE YOU`LL NOT FIND A HOME OF THIS QUALITY, CLOSE TO THIS...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/featured-listing/8-rhaye-place-st-johns/",
            "specs"=> array(
                "age"=> "New",
                "bedrooms"=> 3,
                "bathrooms"=> 2,
                "halfbathrooms"=> 0,
                "building_type"=> null,
                "style"=> "Bungalow",
                "exterior_size"=> null,
                "heating_type"=> "Electric, Heat Pump",
                "interior_size"=> 1188
            )
        ),
        "22953"=> array(
            "id"=> 22953,
            "price"=> 550000,
			"street" =>"10 Little Belle Pl",
			"city" =>"Conception Bay South",
			"province" => "NL",
            "address"=> "10 Little Belle Pl, Conception Bay South",
            "lat"=> 47.516418132475,
            "lng"=> -52.931206226349,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1405641600,
            "description"=> "We think you`ll love this 2970 SQ FT, developed executive bungalow at Oceanview Ridge. The 1485 SQ FT main floor is bathed in natural light and contains...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/featured-listing/10-little-belle-pl-conception-bay-south/",
            "specs"=> array(
                "age"=> "New",
                "bedrooms"=> 3,
                "bathrooms"=> 2,
                "halfbathrooms"=> 1,
                "building_type"=> null,
                "style"=> "Bungalow",
                "exterior_size"=> null,
                "heating_type"=> "Electric",
                "interior_size"=> "2970 + 354"
            )
        ),
        "22961"=> array(
            "id"=> 22961,
            "price"=> 534900,
			"street" =>"1 Little Belle Pl",
			"city" =>"Conception Bay South",
			"province" => "NL",			
            "address"=> "1 Little Belle Pl, Conception Bay South",
            "lat"=> 47.5160703219,
            "lng"=> -52.931098937988,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1405641600,
            "description"=> "Just listed a superbly constructed home in CBS by Classic Contracting . The main floor boasts an open concept kitchen with island, dining area and great...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/featured-listing/1-little-belle-pl-conception-bay-south/",
            "specs"=> array(
                "age"=> "New",
                "bedrooms"=> 3,
                "bathrooms"=> 2,
                "halfbathrooms"=> 1,
                "building_type"=> null,
                "style"=> "2 Level",
                "exterior_size"=> null,
                "heating_type"=> "Electric, Baseboard",
                "interior_size"=> 2040
            )
        ),
        "24159"=> array(
            "id"=> 24159,
            "price"=> 89900,
			"street" =>"10 Williams Hill",
			"city" =>"New Harbour, Trinity Bay",
			"province" => "NL",
            "address"=> "10 Williams Hill, New Harbour, Trinity Bay",
            "lat"=> 47.598494836702,
            "lng"=> -53.544402122498,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1407974400,
            "description"=> "Located in scenic New Harbour is this well kept 2 bedroom cabin on a very large lot. Built by the owners, this cabin is nestled in the tress with beautiful...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/featured-listing/10-williams-hill-new-harbour-trinity-bay/",
            "specs"=> array(
                "age"=> 25,
                "bedrooms"=> 2,
                "bathrooms"=> 1,
                "halfbathrooms"=> 0,
                "building_type"=> null,
                "style"=> "Cabin",
                "exterior_size"=> null,
                "heating_type"=> "Electric, Baseboard",
                "interior_size"=> 576
            )
        ),
        "26499"=> array(
            "id"=> 26499,
            "price"=> 135000,
			"street" =>"101 Springdate Street",
			"city" =>"St. John's",
			"province" => "NL",
            "address"=> "101 Springdale Street, St. John's",
            "lat"=> 47.55773320808,
            "lng"=> -52.71716594696,
            "images"=> array(
                "thumb"=> "https://placekitten.com/g/500/500",
                "gallery"=> array()
            ),
            "list_date"=> 1412726400,
            "description"=> "Excellent opportunity to construct your dream home in a vibrant, bright, up and coming area of downtown, St. John\'s. This building lot will allow for a...",
			"list_brokerage" => "COLDWELL BANKER PROCO",
            "permalink"=> "http=>//www.hanlonrealty.ca/featured-listing/101-springdale-street-st-johns/",
            "specs"=> array(
                "age"=> "",
                "bedrooms"=> 0,
                "bathrooms"=> 0,
                "halfbathrooms"=> 0,
                "building_type"=> null,
                "style"=> "Other",
                "exterior_size"=> null,
                "heating_type"=> "",
                "interior_size"=> ""
            )
        )
    );
	 
	echo json_encode(array('properties'=>$properties));