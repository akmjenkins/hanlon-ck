<button class="toggle-nav">
	<span>&nbsp;</span> Menu
</button>

<div class="nav-wrap">
	
	<div class="sw nav">
	
		<nav>
			<ul>
				<li>
					<a href="#">Buy</a>
					<div class="nav-dd d-bg">
						
						<ul class="nav-dd-links">
							<li><a href="#">All Homes</a></li>
							<li><a href="#">Soon to Market</a></li>
							<li><a href="#">New Construction</a></li>
							<li><a href="#">Accessible Homes</a></li>
							<li><a href="#">Exclusive Listings</a></li>
							<li><a href="#">New Price</a></li>
							<li><a href="#">Land Banks</a></li>
							<li><a href="#">Open Houses</a></li>
						</ul>

						<div class="nav-dd-stats">

							<div class="nav-dd-stat">
								3133
								<span>Listings</span>
							</div><!-- .nav-dd-stat -->

							<div class="nav-dd-stat">
								39
								<span>Featured</span>
							</div><!-- .nav-dd-stat -->

						</div><!-- .nav-dd-stats -->

						<div class="nav-dd-full">
							
							<a href="#" class="button outline">Sign Up</a>
							<span>Search, filter, add homes, updates, news</span>

						</div><!-- .nav-dd-full -->

					</div><!-- .nav-dd -->
				</li>
				<li><a href="#">Sell</a></li>
				<li><a href="#">Mortgages</a></li>
				<li><a href="#">Realtors&reg;</a></li>
				<li><a href="#">Advice</a></li>
			</ul>
		</nav>

		<div class="nav-meta">
			<span class="nav-meta-phone">1 709 738 6200</span>
			<a href="inc/i-sign-in-modal.php" class="nav-meta-login mpopup" data-type="ajax">Login</a>
			
			<div class="t-fa fa-question-circle contact-item">
				<div class="contact-card">
					<div>
						<span class="title">Contact Us</span>
						
						<address>
							1 Crosbie Place <br />
							St. John's, NL A1B 3Y8
						</address>
						
						<span class="phone">
							<span>Ph</span>(709) 123-4567
						</span><!-- .phone -->
						
						<span class="phone">
							<span>TF</span>(800) 123-4567
						</span><!-- .phone -->
					</div>
				</div><!-- .contact-card -->
			</div><!-- .t-fa -->
			
		</div><!-- .nav-meta -->
	
	</div><!-- .nav -->

</div><!-- .nav -->