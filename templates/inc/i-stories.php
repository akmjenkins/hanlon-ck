<div class="split-block full with-border stories-block">

	<div class="split-block-item">
		<div class="split-block-content">
			
			<article>

				<div class="story-snippet article-body">
					<h2>Work With the Space You Are Given</h2>

					<span class="story-meta">
						<a href="#" class="inline">Stories</a> &mdash; <time datetime="2014-12-12" pubdate>December 12, 2014</time>
					</span><!-- .story-meta -->

					<p>
						Pellentesque sagittis, magna sed commodo tempus, orci odio feugiat sem, 
						id facilisis elit urna vitae elit. Proin eleifend justo dui, eget bibendum urna 
						tristique sed. Fusce malesuada, arcu ut hendrerit efficitur, lorem turpis semper 
						justo, eu feugiat sem leo eu nibh. Etiam et placerat massa, et bibendum nisi.
					</p>

					<a href="#" class="button outline">Learn More</a>

				</div><!-- .story-snippet -->

			</article>

		</div><!-- .split-block-content -->
	</div><!-- .split-block-item -->

	<div class="split-block-item">
		<div class="split-block-content">
			
			<article>

				<div class="story-snippet article-body">
					<h2>Work With the Space You Are Given</h2>

					<span class="story-meta">
						<a href="#" class="inline">Stories</a> &mdash; <time datetime="2014-12-12" pubdate>December 12, 2014</time>
					</span><!-- .story-meta -->

					<p>
						Pellentesque sagittis, magna sed commodo tempus, orci odio feugiat sem, 
						id facilisis elit urna vitae elit. Proin eleifend justo dui, eget bibendum urna 
					</p>

					<a href="#" class="button outline">Learn More</a>

				</div><!-- .story-snippet -->

			</article>

		</div><!-- .split-block-content -->
	</div><!-- .split-block-item -->	

</div><!-- .split-block -->