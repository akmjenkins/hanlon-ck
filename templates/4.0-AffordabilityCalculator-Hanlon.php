<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper"
			data-arrows="false" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-1.jpg"></div>

					<div class="hero-content-wrap">

						<div class="hgroup">
							<h1 class="hgroup-title">New Generation of Realtors</h1>
							<span class="hgroup-subtitle">technology + engagement</span>
						</div><!-- .hgroup -->

					</div><!-- .hero-content -->

			</div><!-- .swipe-item -->
				
		</div><!-- .swiper-->
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
			
			<div class="affordability-calculator-wrap">
			
				<div class="affordability-calculator">
				
					<div class="affordability-calculator-info">
					
						<h2>Affordability Calculator</h2>
						
						<p>
							Use this calculator to determine how much house you can afford. By entering details about your income, 
							down payment, and monthly debts, you can estimate the mortgage amount that works with your budget.
						</p>
						
						<div class="affordability-calculator-info-buttons">
							<a href="#" class="button">Get Pre-Approved</a>
							<div class="lazybg with-img" data-src="../assets/images/temp/acme-logo.png"></div>
						</div><!-- .affordability-calculator-info-buttons -->
					
					</div><!-- .affordability-calculator-info -->
					
					<div class="affordability-calculator-fields">
					
						<form action="/" class="body-form">
							<div class="fieldset">
							
								<label>
									Annual Income <i class="t-fa fa-question-circle tooltip" data-content="Tool tip content text goes here"></i>
									<input type="text" name="annual-income">
								</label>
								
								<label>
									Monthly Debt <i class="t-fa fa-question-circle tooltip" data-content="Tool tip content text goes here"></i>
									<input type="text" name="monthly-debt">
								</label>
								
								<label>
									Down Payment <i class="t-fa fa-question-circle tooltip" data-content="Tool tip content text goes here"></i>
									<input type="text" name="down-payment">
								</label>
							
								<a href="#link" class="inline">Advanced &raquo;</a>
							
							</div><!-- .fieldset -->
						</form><!-- .body-form -->
					
					</div><!-- .affordability-calculator-fields -->			
				
				</div><!-- .affordability-calculator -->

				<div class="affordability-calculator-results">
					
					<h2>You can afford up to</h2>
					<div class="lazybg with-img" data-src="../assets/images/temp/acme-logo.png"></div>
					<span class="affordability-calculator-price">$199,463</span>
					
					<p>Based on your income, a house at this price should fit comfortably within your budget.</p>
					
					<a href="#" class="button">Download Report</a>
					<a href="#" class="button">Get Pre-Approved</a>
					
				</div><!-- .affordability-calculator-results -->
		
			</div><!-- .affordability-calculator-wrap -->
		
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section class="img-side-block">
		<div class="sw">
			<div class="img-side-content">
			
				<h2>Vivamus vehicula mauris leo, eu vehicula ipsum tempor non.</h2>
			
				<p>
					Pellentesque sagittis, magna sed commodo tempus, orci odio feugiat sem, id facilisis elit urna vitae elit. 
					Proin eleifend justo dui, eget bibendum urna tristique sed. Fusce malesuada, arcu ut hendrerit efficitur, 
					lorem turpis semper justo, eu feugiat sem leo eu nibh. Etiam et placerat massa, et bibendum nisi. 
				</p>
				
			</div><!-- .img-side-content -->
			
			<div class="img-side-img lazybg" data-src="../assets/images/temp/home-2.jpg">
			</div><!-- .img-side-img -->
		</div><!-- .sw -->
	</section><!-- .img-side-block -->

	<section class="d-bg primary-bg nopad">
		<div class="sw">
			<?php include('inc/i-advice-tools-inside.php'); ?>
		</div><!-- .sw -->
	</section><!-- .d-bg -->	
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>