<?php $bodyclass = 'listing-single-page'; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="lighter-bg">
		<div class="sw">
		
			<div class="hgroup listing-single-title">
				<h1 class="hgroup-title">Listing Price $459,900</h1>
				<h2 class="h4-style hgroup-subtitle">76 Yellowwood Drive, Paradise A1L 0X9</h2>
			</div><!-- .hgroup -->
			
			<div class="listing-single-wrap">
			
				<div class="listing-single">
					<div class="lazybg">
						<img src="../assets/images/temp/home-4.jpg" alt="76 Yellowwood Drive, Paradise">
						
						<!-- add HTML elements for all photos in the gallery as below -->
						<a href="../assets/images/temp/home-4.jpg" data-gallery="76-yellowwood-drive-gallery" data-title="house Photo #1" class="mpopup button fill t-fa ico-after fa-camera">Photos</a>
						<span class="mpopup" data-src="../assets/images/temp/home-4.jpg" data-gallery="76-yellowwood-drive-gallery" data-title="House Photo #2"></span>
						<span class="mpopup" data-src="../assets/images/temp/home-4.jpg" data-gallery="76-yellowwood-drive-gallery" data-title="House Photo #3"></span>
						<span class="mpopup" data-src="../assets/images/temp/home-4.jpg" data-gallery="76-yellowwood-drive-gallery" data-title="House Photo #4"></span>
						<span class="mpopup" data-src="../assets/images/temp/home-4.jpg" data-gallery="76-yellowwood-drive-gallery" data-title="House Photo #5"></span>
						
					</div><!-- .lazybg -->
					
					<div class="listing-single-info">
					
						<div class="listing-single-stat-bar-wrap">
						
							<div class="listing-single-stat-bar">	
								
								<span class="listing-single-stat">
									<span>2</span>
									beds
								</span><!-- .listing-single-stat -->
								
								<span class="listing-single-stat">
									<span>3</span>
									baths
								</span><!-- .listing-single-stat -->
								
								<span class="listing-single-stat">
									<span>1</span>
									half baths
								</span><!-- .listing-single-stat -->
								
								<span class="listing-single-stat">
									<span>1,543</span>
									Sq. Ft.
								</span><!-- .listing-single-stat -->
								
							</div><!-- .listing-single-stat-bar -->
							
							<a href="#" class="toggle-favorite">
								Save
							</a><!-- .toggle-favorite -->
							
						</div><!-- .listing-single-stat-bar-wrap -->
						
						<p>
							Just as you enter the town of Pouch Cove, you will find this brand new and beautifully finished two-apartment home 
							with attached double garage. Located on a large and level lot, the main unit has approximately 1600 square feet of 
							living space with crown moulding throughout. The living room leads into a huge eat-in kitchen that has an island, 
							granite sink and tonnes of beautiful cabinetry. Off the kitchen, you can access the back deck as well as the 
							mudroom/laundry room and garage. The master bedroom has an ensuite with jetted tub and stand-up shower. 
							Two additional bedrooms and a bathroom round off the main floor. Downstairs you will find a family room and 
							another bathroom. The two bedroom apartment has above ground windows, its own laundry room and separate 
							driveway. Rear access. A rare opportunity, don't miss out on this one! All measurements to be verified by purchaser.
						</p>
						
					</div><!-- .listing-single-info -->
				
				</div><!-- .listing-single -->
				
				<div class="listing-mortgage">
				
					<div class="mortgage-item">
					
						<span class="mortgage-item-rate-wrap">
							<span class="mortgage-item-rate">2.79%</span>
							<span class="mortgage-item-term">5 years closed fixed</span>
						</span><!-- .mortgage-item-rate -->
						
						<span class="mortgage-item-amount">
							$1,498.76/mth
						</span><!-- .mortgage-item-amount -->
						
					</div><!-- .mortgage-item -->
					
					<div class="mortgage-item">
					
						<span class="mortgage-item-rate-wrap">
							<span class="mortgage-item-rate">2.20%</span>
							<span class="mortgage-item-term">5 years closed variable</span>
						</span><!-- .mortgage-item-rate -->
						
						<span class="mortgage-item-amount">
							$1,498.76/mth
						</span><!-- .mortgage-item-amount -->
						
					</div><!-- .mortgage-item -->
					
					<div class="mortgage-item">
					
						<span class="mortgage-item-rate-wrap">
							<span class="mortgage-item-rate">2.49%</span>
							<span class="mortgage-item-term">3 years closed fixed</span>
						</span><!-- .mortgage-item-rate -->
						
						<span class="mortgage-item-amount">
							$1,498.76/mth
						</span><!-- .mortgage-item-amount -->
						
					</div><!-- .mortgage-item -->
					
					<div class="mortgage-buttons">
					
						<a href="#" class="t-fa fa-bar-chart">
							<span>How much <br />can I afford?</span>
						</a><!-- .button -->
						
						<a href="#" class="t-fa fa-home">
							<span>What's my <br />monthly payment?</span>
						</a><!-- .button -->
						
					</div><!-- .mortgage-buttons -->
					
				</div><!-- .mortgage-wrap -->
			
			</div><!-- .listing-single-wrap -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="d-bg secondary-bg">
		<div class="sw">
		
			<div class="center">
				<h2>Book a Showing of this House</h2>
				
				<span class="book-showing-inline-input-wrap t-fa fa-calendar">
					<input type="text" class="date-input book-showing-inline-input" 
						data-format="l, F d, Y" 
						data-timepicker="false" 
						data-min-date="0" 
						data-close-on-date-select="true"
						min="
							<?php 
								$today = new DateTime('now',new DateTimeZone('NDT'));
								echo $today->format('Y-m-d'); 
							?>T00:00:00Z"
					>
				</span>
				<button class="button mpopup" data-type="inline" data-src=".book-showing-popup">Book</button>
				
				<div class="book-showing-popup-wrap">
					<div class="book-showing-popup d-bg primary-bg">
						
						<form action="/" class="body-form">
							<div class="fieldset">
							
								<label class="field-wrap book-showing-date-input-wrap">
									<input type="text" 
										class="date-input" 
										name="date"
										placeholder="Choose a Date"
										data-format="l, F d, Y" 
										data-timepicker="false" 
										data-min-date="0" 
										data-close-on-date-select="true"
										min="
											<?php 
												$today = new DateTime('now',new DateTimeZone('NDT'));
												echo $today->format('Y-m-d'); 
											?>T00:00:00Z"
									>
								</label><!-- .field-wrap -->
								
								<input type="text" name="name" placeholder="Name">
								<input type="email" name="email" placeholder="Email Address">
								<input type="tel" name="phone" placeholder="Phone Number">
								<textarea name="message" placeholder="Message" cols="30" rows="10"></textarea>
								
								<button class="button">Submit</button>
								
							</div><!-- .fieldset -->
						</form><!-- .body-form -->
						
					</div><!-- .book-showing-popup -->
				</div><!-- .book-showing-popup-wrap -->
				
			</div><!-- .center -->
		
		</div><!-- .sw -->
	</section><!-- .secondary-bg -->
	
	<section class="lighter-bg">
		<div class="sw">
		
				<div class="inline-realtor">

					<!-- realtor image size must have an aspect ratio of 65% (or 13:20) e.g. 260x400 -->
					<div class="realtor-img">
						<div class="lazybg" data-src="../assets/images/temp/realtor.png"></div>
					</div><!-- .realtor-img -->

					<div class="realtor-info">
						<span class="realtor-name h3-style">Allison Hull</span>
						
						<p>
							Pellentesque sagittis, magna sed commodo tempus, orci odio 
							feugiat sem, id facilisis elit urna vitae elit. Proin eleifend justo dui.
						</p>

						<a href="#" class="button">Send Note</a>
						<a href="#" class="button">Chat Now</a>
						<a href="#" class="button">Schedule Now</a>
						
						<small class="block">Listed by COLDWELL BANKER PROCO</small>
					</div><!-- .realtor-info -->

				</div><!-- .inline-realtor -->
		
		</div><!-- .sw -->
	</section><!-- .lighter-bg -->
	
	<section>
		<div class="sw">
		
			<div class="listing-single-data">
				
				<div class="listing-single-basic-info">
				
					<h2>Basic Information</h2>
					<ul>
						<li class="row">
							<span class="l">MLS&reg;#/PID</span>
							<span class="r">1112121</span>
						</li><!-- .row -->
						<li class="row">
							<span class="l">Address</span>
							<span class="r">576 CONCEPTION BAY Highway</span>
						</li><!-- .row -->
						<li class="row">
							<span class="l">City</span>
							<span class="r">Holyrood</span>
						</li><!-- .row -->
						<li class="row">
							<span class="l">Postal</span>
							<span class="r">A0A2R0</span>
						</li><!-- .row -->
						<li class="row">
							<span class="l">Status</span>
							<span class="r">Active</span>
						</li><!-- .row -->
						<li class="row">
							<span class="l">Style</span>
							<span class="r">Detached</span>
						</li><!-- .row -->
						<li class="row">
							<span class="l">Listed</span>
							<span class="r">Jan 29, 2015</span>
						</li><!-- .row -->
						<li class="row">
							<span class="l">Last Listed Price</span>
							<span class="r">$319,900</span>
						</li><!-- .row -->
						<li class="row">
							<span class="l">Lease</span>
							<span class="r">$0</span>
						</li><!-- .row -->
						<li class="row">
							<span class="l">Subdivision</span>
							<span class="r">N/A</span>
						</li><!-- .row -->
						<li class="row">
							<span class="l">Zoning</span>
							<span class="r">Res</span>
						</li><!-- .row -->
					</ul>

				</div><!-- .listing-single-basic-info -->
				
				<div class="listing-single-rooms">
				
					<h2>Rooms</h2>
					
					<div class="rooms">
					
						<div class="rooms-head room">
							<span>Floor</span>
							<span>Room</span>
							<span>Size</span>
						</div><!-- .rooms-head -->
						
						<ul>
							<li class="room">
								<span>Main Level</span>
								<span>Bath (# pieces 1-6)</span>
								<span>B4</span>
							</li>
							<li class="room">
								<span>Main Level</span>
								<span>Bedroom</span>
								<span>15.40 x 12.60</span>
							</li>
							<li class="room">
								<span>Main Level</span>
								<span>Bedroom</span>
								<span>13.00 x 10.60</span>
							</li>
							<li class="room">
								<span>Main Level</span>
								<span>Bedroom</span>
								<span>10.00 x 8.00</span>
							</li>
							<li class="room">
								<span>Main Level</span>
								<span>Ensuite (# pieces 2-6)</span>
								<span>E4</span>
							</li>
							<li class="room">
								<span>Main Level</span>
								<span>Foyer</span>
								<span>6.00 x 10.00</span>
							</li>
							<li class="room">
								<span>Main Level</span>
								<span>Kitchen</span>
								<span>18.00 x 15.60</span>
							</li>
							<li class="room">
								<span>Main Level</span>
								<span>Living Room/Fireplace</span>
								<span>12.00 x 18.00</span>
							</li>
							<li class="room">
								<span>Main Level</span>
								<span>Master Bedroom</span>
								<span>13.00 x 16.00</span>
							</li>
						</ul>
						
					</div><!-- .rooms -->
					
					<div class="parking">
					
						<div class="parking-space parking-head">
							<span>Name</span>
							<span>Space</span>
						</div><!-- .parking-space -->
						
						<div class="parking-space">
							<span>* Parking Information N/A *</span>
						</div><!-- .parking-space -->
						
						<!-- If there was any parking information, it would look like this: -->
						<!-- 
						<div class="parking-space">
							<span>Name of Parking Space</span>
							<span>Space of Parking Space?</span>
						</div>
						-->
						
					</div><!-- .parking -->

				</div><!-- .listing-single-rooms -->
				
			</div><!-- .listing-single-info -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="d-bg secondary-bg">
		<div class="sw">
			
			<div class="hgroup centered">
				<h2 class="hgroup-title">Property Information</h2>
			</div><!-- .hgroup.centered -->
			
			<div class="property-information">
				<ul>
					<li class="row">
						<span class="l">Type</span>
						<span class="r">Single Family</span>
					</li><!-- .row -->
					<li class="row">
						<span class="l">Style</span>
						<span class="r">Detached, Bungalow</span>
					</li><!-- .row -->
					<li class="row">
						<span class="l">Waterfront</span>
						<span class="r">No</span>
					</li><!-- .row -->
					<li class="row">
						<span class="l">Water Access</span>
						<span class="r">No</span>
					</li><!-- .row -->
					<li class="row">
						<span class="l">Heating</span>
						<span class="r">Electric</span>
					</li><!-- .row -->
					<li class="row">
						<span class="l">Drinking Water Source</span>
						<span class="r">Dug</span>
					</li><!-- .row -->
					<li class="row">
						<span class="l">Sewer</span>
						<span class="r">Municipal</span>
					</li><!-- .row -->
					<li class="row">
						<span class="l">Roof</span>
						<span class="r">Asphalt Shingle</span>
					</li><!-- .row -->
					<li class="row">
						<span class="l">Building Age</span>
						<span class="r">49</span>
					</li><!-- .row -->
					<li class="row">
						<span class="l">PCDS</span>
						<span class="r">Yes</span>
					</li><!-- .row -->
					<li class="row">
						<span class="l">Services</span>
						<span class="r">Electricity, Telephone, Cable, Bus Service, High Speed Internet</span>
					</li><!-- .row -->
					<li class="row">
						<span class="l">Exterior</span>
						<span class="r">Shingles</span>
					</li><!-- .row -->
					<li class="row">
						<span class="l">Driveway</span>
						<span class="r">Paved</span>
					</li><!-- .row -->
					<li class="row">
							<span class="l">Foundation</span>
							<span class="r">Block</span>
					</li><!-- .row -->
				</ul>
			</div><!-- .property-information -->
			
		</div><!-- .sw -->
	</section><!-- .d-bg.secondary-bg -->
	

	<section class="d-bg primary-bg nopad">
		<div class="sw">
			<?php include('inc/i-advice-tools-inside.php'); ?>
		</div><!-- .sw -->
	</section><!-- .d-bg -->	
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>