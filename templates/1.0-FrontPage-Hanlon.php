<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper"
			data-arrows="false" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-1.jpg"></div>	
				
				<div class="hero-content-wrap">
				
					<div class="hero-content">

						<h2>Full-Service Realtors that focuses on getting you the right home at the right price.</h2>

						<div class="hero-buttons">
							<a href="#" class="button big primary fill">Find Your Home</a>
							<a href="#" class="button big primary fill">Sell Your Home</a>
						</div><!-- .hero-buttons -->

						<form action="/" class="single-form">
							<div class="fieldset">
								<input type="text" name="" placeholder="Location, Address, Postal Code or MLS® #">
								<button class="t-fa-abs fa-search">Search</button>
							</div><!-- .fieldset -->
						</form><!-- .single-form -->

					</div><!-- .hero-content -->

					<div class="hero-realtor">

						<!-- realtor image size must have an aspect ratio of 65% (or 13:20) e.g. 260x400 -->
						<div class="realtor-img lazybg" data-src="../assets/images/temp/realtor.png"></div>

						<div class="realtor-info">
							<span class="realtor-name" data-pre="Hanlon Realtor">
								Allison Hull
							</span>

							<a href="#" class="button block ico-after t-fa fa-play-circle-o">Play Video</a>
							<a href="#" class="button fill block ico-after t-fa fa-comments-o">Chat Now</a>
						</div><!-- .realtor-info -->

					</div><!-- .hero-realtor -->

				</div><!-- .hero-content-wrap -->

				
			</div><!-- .swipe-item -->
		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section class="d-bg secondary-bg">
		<div class="sw">
			<?php include('inc/i-featured-properties.php'); ?>
		</div><!-- .sw -->
	</section><!-- .d-bg -->

	<section class="nopad lighter-bg">
		<div class="sw">
			<?php include('inc/i-stories.php'); ?>
		</div><!-- .sw -->
	</section><!-- .d-bg -->	

	<section class="d-bg primary-bg nopad">
		<div class="sw">
			<?php include('inc/i-app-callout.php'); ?>
		</div><!-- .sw -->
	</section><!-- .d-bg -->	

	<section>
		<div class="sw">
			
			<div class="center scroll-down">
			
				<div class="section-title hgroup centered">
					<h2>Hanlon Realty</h2>
					<span class="h4-style hgroup-subtitle">Full-Service Realtors that focus on honest customer service with integrity.</span>
				</div><!-- .section-title -->

				<a href="#" class="button outline">See Our Story</a>
			
			</div><!-- .center -->

		</div><!-- .sw -->
	</section><!-- .d-bg -->	

	<section class="d-bg secondary-bg nopad">
		<div class="sw">
			<?php include('inc/i-advice-tools.php'); ?>
		</div><!-- .sw -->
	</section><!-- .d-bg -->	

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>