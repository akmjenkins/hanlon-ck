<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper"
			data-arrows="false" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-1.jpg"></div>

				<div class="hero-content-wrap">

					<div class="hgroup">
						<h1 class="hgroup-title">New Generation of Realtors</h1>
						<span class="hgroup-subtitle">technology + engagement</span>
					</div><!-- .hgroup -->

				</div><!-- .hero-content -->

			</div><!-- .swipe-item -->
				
		</div><!-- .swiper-->
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section class="dark-bg secondary-bg">
		<div class="sw">
		
			<div class="snippet-block">
		
				<div class="hgroup centered">
					<h2 class="hgroup-title">Vivamus vehicula mauris leo, eu vehicula ipsum tempor non.</h2>
				</div><!-- .hgroup -->
				
				<p>
					Pellentesque sagittis, magna sed commodo tempus, orci odio feugiat sem, id facilisis elit urna vitae elit. 
					Proin eleifend justo dui, eget bibendum urna tristique sed. Fusce malesuada, arcu ut hendrerit efficitur, 
					lorem turpis semper justo, eu feugiat sem leo eu nibh. Etiam et placerat massa, et bibendum nisi. 
				</p>
			
			</div><!-- .snippet-block -->
		
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section class="img-side-block">
		<div class="sw">
			<div class="img-side-content">
			
				<h2>Vivamus vehicula mauris leo, eu vehicula ipsum tempor non.</h2>
			
				<p>
					Pellentesque sagittis, magna sed commodo tempus, orci odio feugiat sem, id facilisis elit urna vitae elit. 
					Proin eleifend justo dui, eget bibendum urna tristique sed. Fusce malesuada, arcu ut hendrerit efficitur, 
					lorem turpis semper justo, eu feugiat sem leo eu nibh. Etiam et placerat massa, et bibendum nisi. 
				</p>
				
			</div><!-- .img-side-content -->
			
			<div class="img-side-img lazybg" data-src="../assets/images/temp/home-3.jpg">
			</div><!-- .img-side-img -->
		</div><!-- .sw -->
	</section><!-- .img-side-block -->
	
	<section class="img-side-block">
		<div class="sw">
			<div class="img-side-content">
			
				<h2>Vivamus vehicula mauris leo, eu vehicula ipsum tempor non.</h2>
			
				<p>
					Pellentesque sagittis, magna sed commodo tempus, orci odio feugiat sem, id facilisis elit urna vitae elit. 
					Proin eleifend justo dui, eget bibendum urna tristique sed. Fusce malesuada, arcu ut hendrerit efficitur, 
					lorem turpis semper justo, eu feugiat sem leo eu nibh. Etiam et placerat massa, et bibendum nisi. 
				</p>
				
			</div><!-- .img-side-content -->
			
			<div class="img-side-img lazybg" data-src="../assets/images/temp/home-2.jpg">
			</div><!-- .img-side-img -->
		</div><!-- .sw -->
	</section><!-- .img-side-block -->

	<section class="d-bg primary-bg nopad">
		<div class="sw">
			<?php include('inc/i-advice-tools-inside.php'); ?>
		</div><!-- .sw -->
	</section><!-- .d-bg -->	
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>